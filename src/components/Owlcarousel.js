import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import React from 'react'

export default function Owlcarousel() {
    return (
        <OwlCarousel className="owl-theme owl-carousel2">
            <div className="item">
                <img src="assets/carousel/img1.png" alt="img" />
            </div>
            <div className="item">
                <img src="assets/carousel/img2.png" alt="img" />
            </div>
            <div className="item">
                <img src="assets/carousel/img3.png" alt="img" />
            </div>
            <div className="item">
                <img src="assets/carousel/img1.png" alt="img" />
            </div>
            <div className="item">
                <img src="assets/carousel/img2.png" alt="img" />
            </div>
            <div className="item">
                <img src="assets/carousel/img3.png" alt="img" />
            </div>
        </OwlCarousel>
    )
}
